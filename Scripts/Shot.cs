﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
  private Rigidbody2D rb;

  public float destroyTimer = 5f;


  private void Awake()
  {
    rb = gameObject.GetComponent<Rigidbody2D>();
  }

  private void Update()
  {
    destroyTimer -= Time.deltaTime;
    if(destroyTimer <= 0f)
    {
      Destroy(this.gameObject);
    }
  }

  private void OnTriggerEnter2D(Collider2D collision)
  {
    rb.velocity = Vector2.zero;

    Demon01 demon = collision.gameObject.GetComponent<Demon01>();
    if(demon == null)
    {
      return;
    }

    demon.Hit();
    Destroy(this.gameObject);
  }
}
