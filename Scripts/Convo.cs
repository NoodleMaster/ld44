﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Convo
{
  public readonly string[] strings;
  public readonly bool[] talkers;

  public Convo(string[] strings, bool[] talkers)
  {
    this.strings = strings;
    this.talkers = talkers;
  }
}
