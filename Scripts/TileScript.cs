﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour
{

  // Start is called before the first frame update
  void Start()
  {
    SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    Color color = spriteRenderer.color;
    float randf = Random.Range(0.75f, 1f);
    color.r *= randf;
    color.g *= randf;
    color.b *= randf;
    spriteRenderer.color = color;
  }

  // Update is called once per frame
  void Update()
  {

  }
}
