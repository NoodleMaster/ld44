﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
  public GameObject shotPrefab;
  public GameObject shellPrefab;
  public Slider slider;

  public AudioClip shootAudio;
  public AudioClip jumpAudio;

  private GameObject gunGO;
  private GameObject legGO;
  private GameObject bodyGO;

  private Animator legAnimator;
  private Animator bodAnimator;
  private Animator gunAnimator;

  private AudioSource audioSource;

  private GameManager gm;
  private BoxCollider2D bc;
  private Rigidbody2D rb;

  public float jumpStrength = 1f;
  public float jumpCooldown = 2f;
  public float speed = 1f;
  public float sprintSpeed = 1f;
  public int ammo = 0;
  public int playerStartingAmmo = 0;
  public int firePower = 1;


  private bool crouch = false;
  private bool gunLoaded = true;
  private float jumpTimer = 0f;
  private bool running = false;

  private bool suicide = false;

  // Start is called before the first frame update
  void Start()
  {
    gunGO = GameObject.Find("Player_Gun");
    legGO = GameObject.Find("Player_Legs");
    bodyGO = GameObject.Find("Player_Body");

    legAnimator = legGO.GetComponent<Animator>();
    bodAnimator = bodyGO.GetComponent<Animator>();
    gunAnimator = gunGO.GetComponent<Animator>();

    audioSource = GetComponent<AudioSource>();

    gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    bc = GetComponent<BoxCollider2D>();
    rb = GetComponent<Rigidbody2D>();
  }


  private float suicideTimer = 0f;
  private Vector3 smoothDampVel = new Vector3();

  // Update is called once per frame
  void Update()
  {
    if(gm.IsPlaused())
    {
      rb.velocity = new Vector2(0f, rb.velocity.y);
      running = false;
      legAnimator.SetBool("Running", running);
      return;
    }

    if(suicide)
    {
      gunGO.transform.localPosition = Vector3.SmoothDamp(gunGO.transform.localPosition, new Vector3(0.75f, 0.25f, 0), ref smoothDampVel, 1f);

      Vector3 diff2 = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 0.25f + 1f - Mathf.Min(1f, 0.5f*suicideTimer), 0f) - gunGO.transform.position;
      diff2.Normalize();

      if(diff2.x < 0f)
      {
        gunGO.GetComponent<SpriteRenderer>().flipY = true;
      }
      else
      {
        gunGO.GetComponent<SpriteRenderer>().flipY = false;
      }

      float rot_z2 = Mathf.Atan2(diff2.y, diff2.x) * Mathf.Rad2Deg;
      gunGO.transform.rotation = Quaternion.Euler(0f, 0f, rot_z2);

      suicideTimer += Time.deltaTime;

      if(suicideTimer >= 2.5f)
      {
        gm.ResetPlayer();
        audioSource.clip = shootAudio;
        audioSource.Play();
        gunGO.transform.localPosition = Vector3.zero;
        suicideTimer = 0f;
        suicide = false;
      }

      return;
    }



    Camera.main.transform.position = new Vector3(gameObject.transform.position.x, 5f, -20f);

    Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - gunGO.transform.position;
    diff.Normalize();

    if(diff.x < 0f)
    {
      gunGO.GetComponent<SpriteRenderer>().flipY = true;
      legGO.GetComponent<SpriteRenderer>().flipX = true;
      bodyGO.GetComponent<SpriteRenderer>().flipX = true;
    }
    else
    {
      gunGO.GetComponent<SpriteRenderer>().flipY = false;
      legGO.GetComponent<SpriteRenderer>().flipX = false;
      bodyGO.GetComponent<SpriteRenderer>().flipX = false;
    }

    float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
    gunGO.transform.rotation = Quaternion.Euler(0f, 0f, rot_z);



    if(Input.GetMouseButtonDown(0) && gunLoaded && ammo > 0 && gunAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Gun_Ready"))
    {
      Shoot();
    }
    else if((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.R)) && !gunLoaded)
    {
      Reload();
    }
    
    if(Input.GetKeyDown(KeyCode.F) && gm.progressLevel >= 3)
    {
      Suicide();
      return;
    }


    jumpTimer -= Time.deltaTime;
    if(jumpTimer <= 0f)
    {
      slider.gameObject.SetActive(false);
    }
    else
    {
      slider.gameObject.SetActive(true);
      slider.value = jumpTimer;
    }

    if(Input.GetKey(KeyCode.W) && jumpTimer <= 0f)
    {
      rb.AddForce(new Vector2(0, 1f/Time.fixedDeltaTime * jumpStrength));
      audioSource.clip = jumpAudio;
      audioSource.Play();
      jumpTimer = jumpCooldown;
    }

    if(Input.GetKey(KeyCode.S))
    {
      crouch = true;
      gunGO.transform.localPosition = new Vector3(0f, -0.25f, 0f);
      bc.size = new Vector2(bc.size.x, 0.95f);
      bc.offset = new Vector2(bc.offset.x, -0.27f);
    }
    else
    {
      crouch = false;
      gunGO.transform.localPosition = Vector3.zero;
      bc.size = new Vector2(bc.size.x, 1.45f);
      bc.offset = new Vector2(bc.offset.x, -0.025f);
    }


    if(Input.GetKey(KeyCode.LeftShift))
    {
      if(Input.GetKey(KeyCode.A))
      {
        rb.velocity = new Vector2(-1f * sprintSpeed, rb.velocity.y);
        running = true;
      }
      else if(Input.GetKey(KeyCode.D))
      {
        rb.velocity = new Vector2(1f * sprintSpeed, rb.velocity.y);
        running = true;
      }
      else
      {
        rb.velocity = new Vector2(0f, rb.velocity.y);
        running = false;
      }
    }
    else
    {
      if(Input.GetKey(KeyCode.A))
      {
        rb.velocity = new Vector2(-1f * speed, rb.velocity.y);
        running = true;
      }
      else if(Input.GetKey(KeyCode.D))
      {
        rb.velocity = new Vector2(1f * speed, rb.velocity.y);
        running = true;
      }
      else
      {
        rb.velocity = new Vector2(0f, rb.velocity.y);
        running = false;
      }
    }
    

    legAnimator.SetBool("Running", running);
    bodAnimator.SetBool("Crouch", crouch);
  }



  private void Shoot()
  {
    for(int i = 0; i < 6 * firePower; i++)
    {
      GameObject shotGO = Instantiate(shotPrefab, gunGO.transform.position + gunGO.transform.rotation * new Vector3(0.5f, 0f, 0f), Quaternion.identity);
      shotGO.GetComponent<Rigidbody2D>().velocity = gunGO.transform.rotation * new Vector3(25f, 0f, 0f) + new Vector3(Random.Range(-5f, 5f), Random.Range(-5f, 5f), 0f) + new Vector3(rb.velocity.x, rb.velocity.y, 0f);
      shotGO.transform.localScale = new Vector3(firePower, firePower, firePower);
    }

    GameObject shellGO = Instantiate(shellPrefab, gunGO.transform.position, Quaternion.identity);
    shellGO.GetComponent<Rigidbody2D>().velocity = gunGO.transform.rotation * new Vector3(0f, 2.5f, 0f) + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f) + new Vector3(rb.velocity.x, rb.velocity.y, 0f);
    shellGO.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-1000f, 1000f);
    shellGO.transform.localScale = new Vector3(firePower, firePower, firePower);

    audioSource.clip = shootAudio;
    audioSource.Play();

    ammo--;
    gunLoaded = false;
    Reload();
  }

  private void Reload()
  {
    gunLoaded = true;
    gunAnimator.SetTrigger("Reloading");
  }

  public void Suicide()
  {
    suicide = true;
    rb.velocity = new Vector2(0f, rb.velocity.y);
    running = false;

    if(gm.progressLevel >= 3)
    {
      sprintSpeed = 5f;
    }
    if(gm.progressLevel >= 4)
    {
      jumpStrength = 8f;
    }
    if(gm.progressLevel >= 4)
    {
      jumpStrength = 8f;
    }
    if(gm.progressLevel >= 5)
    {
      firePower += 1;
      playerStartingAmmo += 10;
      ammo = playerStartingAmmo;
    }

    legAnimator.SetBool("Running", running);
  }

}
