﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demon01 : MonoBehaviour
{
  public float attackRange = 10f;
  public float speed = 1f;
  public float attackSpeed = 1f;
  public float health = 3f;

  private Animator animator;
  private Rigidbody2D rb;
  private GameManager gm;
  private Player player;

  private float timer = 0f;
  private bool isAttacking = false;

  // Start is called before the first frame update
  void Start()
  {
    animator = transform.GetChild(0).gameObject.GetComponent<Animator>();
    rb = GetComponent<Rigidbody2D>();
    gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    player = GameObject.Find("Player").GetComponent<Player>();
  }

  // Update is called once per frame
  void Update()
  {
    if(gm.IsPlaused())
    {
      rb.velocity = Vector2.zero;
      return;
    }


    timer += Time.deltaTime;

    Vector2 playerTargetVector = player.transform.position - this.transform.position;

    if(playerTargetVector.magnitude <= attackRange)
    {
      isAttacking = true;
    }
    else
    {
      isAttacking = false;
    }

    if(isAttacking)
    {
      rb.velocity = playerTargetVector.normalized * attackSpeed;
    }
    else if(timer > 1f)
    {
      timer = 0f;

      float randX = Random.Range(-1f, 1f);
      float randY = Random.Range(-1f, 1f);

      if(transform.position.y > 10f)
      {
        randY = Mathf.Abs(randY) * -1f;
      }
      else if(transform.position.y < 2f)
      {
        randY = Mathf.Abs(randY);
      }

      rb.velocity = new Vector2(randX * speed, randY * speed);
    }

    animator.SetBool("Attack", isAttacking);
  }

  public void Hit()
  {
    health--;

    if(health <= 0)
    {
      //Play sound!!!
      this.gameObject.SetActive(false);
    }
  }



  private void OnCollisionEnter2D(Collision2D collision)
  {
    if(collision.gameObject.GetComponent<Player>() != null)
    {
      gm.ResetPlayer();
      gm.StartConvo(new Convo(new string[] { "HAHAHAHAHA...     \nWOW... YOU'RE REALLY BAD AT THIS. TRY HARDER!          " }, new bool[] { false }));
    }
  }

}
