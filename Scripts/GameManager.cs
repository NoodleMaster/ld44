﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
  public Text dialogText;
  public GameObject dialogPanel;
  public GameObject pausePanel;
  public Image satanHeadImage;
  public Image playerHeadImage;
  public Image black;

  public Tilemap tilemap;

  public bool isInDialog = false;

  public int progressLevel = 0;
  public float textSpeed = 5f;


  private Player player;
  private Demon01[] demons;
  private Vector3[] demonStartPositions;

  private bool isPaused = false;

  private bool isPlayerReset = false;
  private float playerResetTimer = 0f;
  private bool isCustomConvo = false;
  private Convo customConvo;
  private int talkerCounter = 0;
  private float timer = 0f;

  // Start is called before the first frame update
  void Start()
  {
    player = GameObject.Find("Player").GetComponent<Player>();

    demons = GameObject.FindObjectsOfType<Demon01>();
    demonStartPositions = new Vector3[demons.Length];
    for(int i = 0; i < demons.Length; i++)
    {
      demonStartPositions[i] = demons[i].transform.position;
    }
  }

  // Update is called once per frame
  void Update()
  {
    if(Input.GetKeyDown(KeyCode.Escape))
    {
      isPaused = !isPaused;
    }

    pausePanel.SetActive(isPaused);
    AudioListener.pause = isPaused;

    if(isPaused)
    {
      return;
    }



    if(isPlayerReset)
    {
      playerResetTimer += Time.deltaTime;

      if(playerResetTimer >= 2f)
      {
        isPlayerReset = false;
        black.enabled = false;
        playerResetTimer = 0f;
        return;
      }
    }


    if(!isInDialog)
    {
      return;
    }

    if(!isCustomConvo && talkerCounter >= convos[progressLevel].strings.Length)
    {
      if(progressLevel == 1)
      {
        //rain shells
        Player player = GameObject.Find("Player").GetComponent<Player>();
        player.ammo = 20;
        player.playerStartingAmmo = 20;
        for(int i = 0; i < 20; i++)
        {
          GameObject gameObject = Instantiate(player.shellPrefab, player.transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-2.5f, 5f)+10, 0f), Quaternion.identity);
          gameObject.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(1000f, -1000f);
        }
      }
      if(progressLevel == 5)
      {
        for(int x = 111; x < 125; x++)
        {
          for(int y = 0; y < 13; y++)
          {
            tilemap.SetTile(new Vector3Int(x, y, 0), null);
          }
        }
      }
      if(progressLevel == 6)
      {
        player.Suicide();
      }
      if(progressLevel == 7)
      {
        for(int x = 138; x < 160; x++)
        {
          for(int y = 1; y < 3; y++)
          {
            tilemap.SetTile(new Vector3Int(x, y, 0), null);
          }
        }
      }
      if(progressLevel >= 8)
      {
        EndGame();
      }

      isInDialog = false;
      dialogPanel.SetActive(false);
      progressLevel++;
      talkerCounter = 0;
      return;
    }
    else if(isCustomConvo && talkerCounter >= customConvo.strings.Length)
    {
      isInDialog = false;
      isCustomConvo = false;
      dialogPanel.SetActive(false);
      talkerCounter = 0;
      return;
    }

    timer += Time.deltaTime;

    if((isCustomConvo && customConvo.talkers[talkerCounter]) || (!isCustomConvo && convos[progressLevel].talkers[talkerCounter]))
    {
      //PlayerHead
      playerHeadImage.enabled = true;
      satanHeadImage.enabled = false;
    }
    else
    {
      //DevilHead
      playerHeadImage.enabled = false;
      satanHeadImage.enabled = true;
    }

    string currentText = "";
    if(isCustomConvo)
    {
      currentText = customConvo.strings[talkerCounter];
    }
    else
    {
      currentText = convos[progressLevel].strings[talkerCounter];
    }
    dialogText.text = GetWords(currentText, (int) (timer * textSpeed));
  }

  private string GetWords(string text, int wordCount)
  {
    if(wordCount > text.Length)
    {
      timer = 0f;
      talkerCounter++;
      return text;
    }
    return text.Substring(0, wordCount);
  }

  public void StartConvo()
  {
    isInDialog = true;
    dialogPanel.SetActive(true);
  }

  public void StartConvo(Convo convo)
  {
    isCustomConvo = true;
    customConvo = convo;
    isInDialog = true;
    dialogPanel.SetActive(true);
  }


  public void ResetPlayer()
  {
    //make black screen
    black.enabled = true;
    isPlayerReset = true;


    //put player back to start
    player.transform.position = new Vector3(1f, 2f, 0f);
    player.ammo = player.playerStartingAmmo;

    //respawn all the enemys
    for(int i = 0; i < demons.Length; i++)
    {
      demons[i].transform.position = demonStartPositions[i];
      demons[i].gameObject.SetActive(true);
      demons[i].health = Mathf.Min(2+progressLevel, 6);
      demons[i].speed += Mathf.Min(0.25f, 4f);
      demons[i].attackSpeed += Mathf.Min(0.5f, 6f);
    }
  }
  
  public bool IsPlaused()
  {
    return isPlayerReset || isInDialog || isPaused;
  }

  public void EndGame()
  {
    SceneManager.LoadScene(2);
  }

  private readonly Convo[] convos = new Convo[]
  {
    new Convo(new string[]{ "...?\nWHERE AM I?                  ", "YOU'RE IN HELL BUDDY.\n\nAND I WILL WATCH YOU FAIL TRYING TO GET OUT.        ", "HAHAHAHAHA...            " }, new bool[]{ true, false, false }),
    new Convo(new string[]{ "OH WAIT.\nHERE IS A LITTLE SOMETHING TO GET YOU STARTED.\nLET'S SEE HOW FAR YOU GET BEFORE YOU NEED MY \"HELP\" AGAIN           " }, new bool[]{ false }),
    new Convo(new string[]{ "...                ", "HOW AM I SUPPOSED TO GET OVER THERE, I CAN'T JUMP THAT FAR!?          ", "DO YOU NEED SOME ...       \nHELP?    \nPRESS 'F' AND I'LL \"HELP\" YOU.       ", "...   OKAY          " }, new bool[]{ false, true, false, true }),
    new Convo(new string[]{ "THIS WOULD BE WAY EASIER IF YOU COULD JUMP HIGHER, WOULD'NT IT?          ", "IT WOULD.....     ", "JUST PRESS THE MAGIC BUTTON.               \nHAHAHAHA...       " }, new bool[]{ false, true, false }),
    new Convo(new string[]{ "THERE ARE A LOT OF THEM JUST WAITING TO EAT YOU UP.\nDO YOU HAVE ENOUGH FIREPOWER OR DO YOU NEED AN UPGRADE?              ", "YOU KNOW WHAT TO DO.    \nHAHAHAHA.....         " }, new bool[]{ false, false }),
    new Convo(new string[]{ }, new bool[]{ }),
    new Convo(new string[]{ "IS THIS THE END?         ", "OH...        \nYOU ACTUALLY MADE IT TO THE END.    \nGOOD FOR YOU.                 ", "SO...         \nCAN I GO NOW?      ", "...           \nNO          ", "???    ","DO IT AGAIN!          " }, new bool[]{ true, false, true, false, true, false}),
    new Convo(new string[]{ "CAN I PLEASE GO NOW?      ", "WELL... YOU DID DO IT AGAIN.\nAND I'M GETTING BORED OF YOU.          \nOK, YOU CAN LEAVE NOW." }, new bool[]{ true, false }),
    new Convo(new string[]{ }, new bool[]{ })
  };
  

}
