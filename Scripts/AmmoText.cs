﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoText : MonoBehaviour
{
  private Text text;

  private void Start()
  {
    text = GetComponent<Text>();
  }

  private void Update()
  {
    text.text = GameObject.Find("Player").GetComponent<Player>().ammo + "";
  }
}
