﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBox : MonoBehaviour
{
  private GameManager gm;
  
  // Start is called before the first frame update
  void Start()
  {
    gm = GameObject.Find("GameManager").GetComponent<GameManager>();
  }

  private void OnTriggerEnter2D(Collider2D collision)
  {
    if(collision.gameObject.GetComponent<Player>() == null)
    {
      return;
    }
    gm.ResetPlayer();
    gm.StartConvo(new Convo(new string[] { "HAHAHAHAHA...     \nYOU ACTUALLY JUMPED INTO THE PIT\n\nHAHAHAHAHA....             " }, new bool[] { false }));
  }
}
