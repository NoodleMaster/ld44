﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConvoTrigger : MonoBehaviour
{
  private GameManager gm;


  // Start is called before the first frame update
  void Start()
  {
    gm = GameObject.Find("GameManager").GetComponent<GameManager>();
  }

  private void OnTriggerEnter2D(Collider2D collision)
  {
    Debug.Log("ConvoTrigger Hit");
    gm.StartConvo();
    Destroy(this.gameObject);
  }
}
